package com.formation.sbrestmysql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.formation.sbrestmysql.entity.User;

public interface UserRepository extends JpaRepository<User,Long> {
	
}
